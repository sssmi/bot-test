// Import the feedback functions
const { message, warn, fail, markdown, danger } = require('danger');

// Add a message to the table
message('You have added 2 more modules to the app');

// Adds a warning to the table
warn('You have not included a CHANGELOG entry.');

// Show markdown under the table:
// markdown('### Work in progress: ' + danger.gitlab.mr.work_in_progress);
// markdown('### Work in progress: ');
